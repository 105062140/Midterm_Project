function init() {
    var user_email = '';
    var loginUser;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            loginUser = firebase.auth().currentUser;
            menu.innerHTML = "<span class='dropdown-item' id='data-btn'>" + user.email + "</span><span class='dropdown-item' id='history-btn'>購買紀錄</span><span class='dropdown-item' id='logout-btn'>登出</span>";
            var logout_button = document.getElementById('logout-btn');
            var data_button = document.getElementById('data-btn');
            var history_button = document.getElementById('history-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                        window.location = "index.html"
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            data_button.addEventListener('click', function () {
                window.location = "data.html"
            });
            history_button.addEventListener('click', function () {
                window.location = "history.html"
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    // ContactsRef.child("Contacts").child("3").setValue(contact3);
    post_btn = document.getElementById('post_btn');
    post_txt1 = document.getElementById('Name');
    post_txt2 = document.getElementById('id_number');
    post_txt3 = document.getElementById('birth');
    post_txt4 = document.getElementById('address');
    post_txt5 = document.getElementById('number');
    post_txt6 = document.getElementById('phone');
    post_btn.addEventListener('click', function () {
        if (post_txt1.value !="" && post_txt2.value !="" && post_txt3.value !="" && post_txt4.value !="") {
            var newpostref = firebase.database().ref('/contact/' + loginUser.uid+'/buy').push();
            newpostref.set({
                thing: "HyperX Savage USB 3.1 128GB 高速隨身碟 (HXS3/128GB) ",
                price: "2199",
                number : post_txt5.value,
                phone: post_txt6.value
            });
            var newpostref1 = firebase.database().ref('/contact/' + loginUser.uid+'/info');
            newpostref1.set({
                name: post_txt1.value,
                idnum: post_txt2.value,
                birth: post_txt3.value,
                address: post_txt4.value
            }).then(function () {
                alert('購買成功!')
                window.location = "index.html"
            }).catch(function () {
                alert('購買失敗!')
                window.location = "index.html"
            });
            post_txt1.value = "";
            post_txt2.value = "";
            post_txt3.value = "";
            post_txt4.value = "";
            post_txt5.value = "";
            post_txt6.value = "";
        }
    });
}

window.onload = function () {
    init();
}