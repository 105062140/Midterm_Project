function init() {
    var user_email = '';
    var loginUser;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            loginUser = firebase.auth().currentUser;
            menu.innerHTML = "<span class='dropdown-item' id='data-btn'>" + user.email + "</span><span class='dropdown-item' id='history-btn'>購買紀錄</span><span class='dropdown-item' id='logout-btn'>登出</span>";
            var logout_button = document.getElementById('logout-btn');
            var data_button = document.getElementById('data-btn');
            var history_button = document.getElementById('history-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                        window.location = "index.html"
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            data_button.addEventListener('click', function () {
                window.location = "data.html"
            });
            history_button.addEventListener('click', function () {
                window.location = "history.html"
            });
            var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_after_content = "</p></div></div>\n";
            var postsRef = firebase.database().ref('/contact/' + loginUser.uid + '/buy');
            var total_post = [];
            postsRef.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    total_post[total_post.length] = str_before_username + childData.thing + "</strong>單價：" + childData.price+"</p><p>數量："+ childData.number + str_after_content;
                });
                document.getElementById('post_list').innerHTML = total_post.join('');
            });

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    
}
window.onload = function () {
    init();
}