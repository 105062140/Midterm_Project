function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='data-btn'>" + user.email + "</span><span class='dropdown-item' id='history-btn'>購買紀錄</span><span class='dropdown-item' id='logout-btn'>登出</span>";
            var logout_button = document.getElementById('logout-btn');
            var data_button = document.getElementById('data-btn');
            var history_button = document.getElementById('history-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            data_button.addEventListener('click', function () {
                window.location = "data.html"
            });
            history_button.addEventListener('click', function () {
                window.location = "history.html"
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('/comment/USB6').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
    });

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('/comment/USB6');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}