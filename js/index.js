function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='data-btn'>" + user.email + "</span><span class='dropdown-item' id='history-btn'>購買紀錄</span><span class='dropdown-item' id='logout-btn'>登出</span>";
            var logout_button = document.getElementById('logout-btn');
            var data_button = document.getElementById('data-btn');
            var history_button = document.getElementById('history-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            data_button.addEventListener('click', function () {
                window.location = "data.html"
            });
            history_button.addEventListener('click', function () {
                window.location = "history.html"
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
}

window.onload = function () {
    init();
    var notifyConfig = {
        body: 'Welcome to the Shopping Web Page', 
    };
    if(Notification.permission === 'default' || Notification.permission === 'undefined'||Notification.permission === 'granted') {
        Notification.requestPermission(function (permission) {
        var notification = new Notification('Hello!', notifyConfig);
        })
    }
}