# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Shopping Web Page]
  網頁部份:https://105062140.gitlab.io/Midterm_Project
* Key functions (add/delete)
    1. [Product page]
    2. [shopping pipeline]
    3. [user dashboard]
* Other functions (add/delete)
    1. [comment]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
* **Signin Signup**
在購物網上的每個頁面左上角(除了signin頁面)，皆可看到account，點擊登入即可進入到登入頁面，
在登入頁面中，如果尚未註冊，那只要輸入E-mail與密碼，在按下signup即可進行註冊，如顯示註冊成功，則再重新輸入E-mail與密碼，按下signin即可進行登入，此外，也提供了google帳號與facebook帳號登入機制，最後，登入成功會跳轉回index頁面
* **Product page**
在index頁面上可看到購物網上共有九項商品可供選購，每一項商品的藍色介紹文字都是一個連結，點擊可進入商品詳細介紹頁面，在此頁面上除了有更詳細的介紹，也可點擊購買
* **shopping pipeline**
承上，點擊了購買之後，則會進到填寫資料與數量的表單頁面，如果是未登入狀態，就算填完了表單，也一樣無法送出
* **user dashboard**
在進行完登入後，左上角的account會顯示三個項目，一個是帳戶的E-mail，點擊可進入帳戶資料頁面，個人的資料取決於填寫購買表單的內容，所以要是填寫表單時個人資料每次都有所變更，那帳戶資料也會跟著變動，第二個則是購買紀錄，點擊進去可看到此帳號的購買紀錄，內容包括商品名稱，單價以及購買數量，最後一欄則是登出
* **Database**
每一次填寫商品購買表單都會將資料送到firebase，而firebase則會根據帳號的uid碼，建立起一個節點，底下的資料又會生成兩個節點，一個節點是購買物品種類數量及價格，由於需記錄所有購買內容，所以是使用push進行資料建立，另一個節點則是帳戶的資料，由於只需記錄最新資料，所以只用set進行資料建立與更新
* **Chrome Notification**
在剛進到頁面時，會跳出通知，對訪客進行問候
* **comment**
已登入的用戶可在商品詳細介紹頁面最下方進行留言
## Security Report (Optional)